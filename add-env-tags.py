import boto3

ec2_client_frankfurt = boto3.client("ec2", region_name="eu-central-1")
ec2_resource_frankfurt = boto3.resource("ec2",region_name="eu-central-1")

ec2_client_paris = boto3.client("ec2", region_name="eu-west-3")
ec2_resource_paris = boto3.resource("ec2",region_name="eu-west-3")

instances_ids_frankfurt = []
instances_ids_paris = []

reservations_instances_frankfurt = ec2_client_frankfurt.describe_instances() ['Reservations']
for res in reservations_instances_frankfurt:
    instances = res['Instances']
    for instance in instances:
        instances_ids_frankfurt.append(instance['InstanceId'])

response = ec2_resource_frankfurt.create_tags(
    Resources=instances_ids_frankfurt,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'prod'
        },
    ]
)

reservations_instances_paris = ec2_client_paris.describe_instances() ['Reservations']
for res in reservations_instances_paris:
    instances = res['Instances']
    for instance in instances:
        instances_ids_paris.append(instance['InstanceId'])

response = ec2_resource_paris.create_tags(
    Resources=instances_ids_paris,
    Tags=[
        {
            'Key': 'environment',
            'Value': 'dev'
        },
    ]
)